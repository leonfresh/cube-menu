﻿using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    public float speed = 0.01f;
    
    private Rigidbody _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        _rb.AddForce(new Vector3(speed, 0,0));
    }
}
