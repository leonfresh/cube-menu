﻿using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public List<Vector3> cameraVectors = new List<Vector3>();
    public List<Vector3> cameraRotations = new List<Vector3>();
    
    private void OnEnable()
    {
        GameEvents.MenuChanged += MenuChanged;
    }

    private void OnDisable()
    {
        GameEvents.MenuChanged -= MenuChanged;
    }
    
    private void MenuChanged(int screen)
    {
        if (Camera.main == null)
            return;

        CustomTween.LerpCamera(cameraVectors[screen], cameraRotations[screen], 1f);
    }
}
