using System.Collections.Generic;
using Kino;
using MEC;
using UnityEngine;

public static class CustomTween
{
    private static readonly AnimationCurve Curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
    public static void AlphaCanvas(CanvasGroup canvasGroup, float endVal, float duration)
    {
        Timing.RunCoroutine(_AlphaCanvas(canvasGroup, endVal, duration));
    }
    private static IEnumerator<float> _AlphaCanvas(CanvasGroup canvasGroup, float endVal, float duration)
    {
        float elapsedTime = 0;

        var start = canvasGroup.alpha;
            
        while (elapsedTime < duration)
        {
            canvasGroup.alpha = Mathf.SmoothStep(start, endVal, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return 0;
        }
        canvasGroup.alpha = endVal;
    }
    
    public static void RotateElement(CanvasGroup canvasGroup, Vector3 endVal, float duration)
    {
        Timing.RunCoroutine(_RotateElement(canvasGroup, endVal, duration));
    }
    private static IEnumerator<float> _RotateElement(CanvasGroup canvasGroup, Vector3 endVal, float duration)
    {
        float elapsedTime = 0;

        var rectTransform = canvasGroup.GetComponent<RectTransform>();
        var start = rectTransform.rotation;
            
        while (elapsedTime < duration)
        {
            rectTransform.rotation = Quaternion.Lerp(start, Quaternion.Euler(endVal.x,endVal.y,endVal.z), elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return 0;
        }
        canvasGroup.transform.rotation = Quaternion.Euler(endVal.x,endVal.y,endVal.z);
    }

    public static void GlitchTween(AnalogGlitch glitch, float endVal, float duration)
    {
        Timing.RunCoroutine(_GlitchTween(glitch, endVal, duration));
    }
    private static IEnumerator<float> _GlitchTween(AnalogGlitch glitch, float endVal, float duration)
    {
        float elapsedTime = 0;

        var start1 = glitch.scanLineJitter;
        var start2 = glitch.verticalJump;
        var start3 = glitch.colorDrift;
            
        while (elapsedTime < duration)
        {
            glitch.scanLineJitter = Mathf.SmoothStep(start1, endVal, elapsedTime / duration);
            glitch.verticalJump = Mathf.SmoothStep(start2, endVal, elapsedTime / duration);
            glitch.colorDrift = Mathf.SmoothStep(start3, endVal, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return 0;
        }
        glitch.scanLineJitter = endVal;
        glitch.verticalJump = endVal;
        glitch.colorDrift = endVal;
    }
    
    public static void LerpCamera(Vector3 endVector, Vector3 endQuaternion, float duration)
    {
        Timing.RunCoroutine(_LerpCamera(endVector, endQuaternion, duration));
    }
    private static IEnumerator<float> _LerpCamera(Vector3 endVector, Vector3 endQuaternion, float duration)
    {
        if (Camera.main == null)
            yield break;
        
        var cameraTransform = Camera.main.transform;
        var startPos = cameraTransform.position;
        var startRot = cameraTransform.rotation;
        
        float elapsedTime = 0;

        while (elapsedTime < duration)
        {
            var s = elapsedTime / duration;

            Camera.main.transform.position = Vector3.Slerp(startPos, endVector, Curve.Evaluate(s));

            Camera.main.transform.rotation = Quaternion.Slerp(startRot, Quaternion.Euler(endQuaternion.x,endQuaternion.y,endQuaternion.z), Curve.Evaluate(s));
            elapsedTime += Time.deltaTime;
            yield return 0;
        }

        cameraTransform.rotation = Quaternion.Euler(endQuaternion.x,endQuaternion.y,endQuaternion.z);
        cameraTransform.position = endVector;
    }
}