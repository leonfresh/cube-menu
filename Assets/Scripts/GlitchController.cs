﻿using System.Collections.Generic;
using Kino;
using MEC;
using UnityEngine;

public class GlitchController : MonoBehaviour
{
    public float glitchAmount = .5f;
    public float glitchDuration = .3f;
    
    private AnalogGlitch _glitch;
    private bool _isGlitchRunning;
    private void Awake()
    {
        _glitch = GetComponent<AnalogGlitch>();
    }
    private void OnEnable()
    {
        GameEvents.MenuChanged += MenuChanged;
    }

    private void OnDisable()
    {
        GameEvents.MenuChanged -= MenuChanged;
    }
    
    private void MenuChanged(int screen)
    {
        if (!_isGlitchRunning)
        {
            Timing.RunCoroutine(_StartAnimation());
        }
    }

    private IEnumerator<float> _StartAnimation()
    {
        _isGlitchRunning = true;
        CustomTween.GlitchTween(_glitch, glitchAmount, glitchDuration);
        yield return Timing.WaitForSeconds(glitchDuration);
        CustomTween.GlitchTween(_glitch, 0f, glitchDuration);
        yield return Timing.WaitForSeconds(glitchDuration);
        _isGlitchRunning = false;
    }
}
