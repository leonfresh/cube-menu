using UnityEngine.Events;

public static class GameEvents
{
    public static event UnityAction<int> MenuChanged = delegate { };

    public static void OnMenuChanged(int screen)
    {
        MenuChanged(screen);
    }

}
