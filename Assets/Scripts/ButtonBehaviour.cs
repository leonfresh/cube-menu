﻿using System.Collections;
using System.Collections.Generic;
using Shapes2D;
using UnityEngine;

public class ButtonBehaviour : MonoBehaviour
{
    public int thisButtonScreenId;

    private Shape _shape;
    
    private void Awake()
    {
        _shape = GetComponent<Shape>();
    }
    private void OnEnable()
    {
        GameEvents.MenuChanged += MenuChanged;
    }
    private void OnDisable()
    {
        GameEvents.MenuChanged -= MenuChanged;
    }
    private void MenuChanged(int screen)
    {
        _shape.settings.outlineSize = thisButtonScreenId == screen ? 4 : 1;
    }
}
