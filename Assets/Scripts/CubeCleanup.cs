﻿using UnityEngine;

public class CubeCleanup : MonoBehaviour
{
    public float expiryTime = 10f;

    private float _timer;

    private Renderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }
    private void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > expiryTime)
        {
            if (!_renderer.isVisible)
            {
                Destroy(gameObject);
            }
        }
    }
}
