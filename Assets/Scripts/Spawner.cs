﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float initialSpawnAmount = 30;
    public float spawnInterval;
    public GameObject objectToSpawn;
    public float maxSpawnAreaX = 10;
    public float maxSpawnAreaY = 10;
    public float maxSpawnAreaZ = 3;

    private float _timer;

    private void Start()
    {
        for (var i = 0; i < initialSpawnAmount; i++)
        {
            Instantiate(objectToSpawn,
                new Vector3(Random.Range(-maxSpawnAreaX, maxSpawnAreaX), Random.Range(-maxSpawnAreaY, maxSpawnAreaY), 
                    Random.Range(-maxSpawnAreaZ, maxSpawnAreaZ)) + new Vector3(20,0,0),
                Quaternion.Euler(Random.Range(0,359), Random.Range(0,359),Random.Range(0,359)), transform);
        }
    }
    
    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > spawnInterval)
        {
            _timer = 0;
            Spawn();
        }
    }

    private void Spawn()
    {
        Instantiate(objectToSpawn,
            new Vector3(Random.Range(-maxSpawnAreaX, maxSpawnAreaX), Random.Range(-maxSpawnAreaY, maxSpawnAreaY), Random.Range(-maxSpawnAreaZ, maxSpawnAreaZ)),
            Quaternion.Euler(Random.Range(0,359), Random.Range(0,359),Random.Range(0,359)), transform);
    }
}
