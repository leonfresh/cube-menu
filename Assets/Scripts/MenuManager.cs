﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public CanvasGroup[] menus;

    public int currentActiveScreen = 0;

    // how many seconds to cooldown after pressing menu change
    public float onScreenChangeDelay = 5;
    private float _timer;

    private void Start()
    {
        Timing.RunCoroutine(_AnimateElements(0));
    }

    private void Update()
    {
        if (_timer > 0)
        {
            _timer -= Time.deltaTime;
        }
    }
    
    public void ChangeScreen(int screen)
    {
        if (screen == currentActiveScreen || _timer > 0)
            return;
        
        GameEvents.OnMenuChanged(screen);
        
        Timing.RunCoroutine(_AnimateElements(screen));
        
        currentActiveScreen = screen;
        _timer = onScreenChangeDelay;
    }

    private IEnumerator<float> _AnimateElements(int screen)
    {
        CustomTween.AlphaCanvas(menus[currentActiveScreen], 0, .25f);

        yield return Timing.WaitForSeconds(.5f);
        
        foreach (Transform t in menus[currentActiveScreen].transform)
        {
            t.GetComponent<CanvasGroup>().alpha = 0;
        }

        menus[screen].alpha = 1;
        foreach (Transform t in menus[screen].transform)
        {
            var cg = t.GetComponent<CanvasGroup>();
            
            CustomTween.AlphaCanvas(cg, 1, .25f);
            
            t.transform.rotation = Quaternion.Euler(0, 180, 0);
            CustomTween.RotateElement(cg, Vector3.zero, .25f);
            
            yield return Timing.WaitForSeconds(.3f);
        }
    }
}
